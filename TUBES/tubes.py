# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer
# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler
import threading
import math
# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)
# Buat server
with SimpleXMLRPCServer(("192.168.43.122", 54321),
                        requestHandler=RequestHandler, allow_none=True) as server:
    server.register_introspection_functions()
    # siapkan lock
    lock = threading.Lock()
    def menu():
        lock.acquire()
        lock.release()
        msg="==============================\n==  SELAMAT DATANG DI CAOL  ==\n==============================\nMenu : \n1. Basic\n2. Luas\n3. Keliling"
        return msg
        lock.release()
    server.register_function(menu)

    def basic():
        lock.acquire()
        lock.release()
        msg="==============================\n==  SELAMAT DATANG DI CAOL  ==\n==============================\n==>Basic\nMenu : \n1. Tambah\n2. Kurang\n3. Kali\n4. Bagi\n0.Menu"
        return msg
        lock.release()
    server.register_function(basic)

    def luas():
        lock.acquire()
        lock.release()
        msg="==============================\n==  SELAMAT DATANG DI CAOL  ==\n==============================\n==>Luas\nMenu : \n1. Persegi\n2. Persegi Panjang\n3. Segitiga\n4. Lingkaran\n0. Menu"
        return msg
        lock.release()
    server.register_function(luas)

    def keliling():
        lock.acquire()
        lock.release()
        msg="==============================\n==  SELAMAT DATANG DI CAOL  ==\n==============================\n==>Keliling\nMenu : \n1. Persegi\n2. Persegi Panjang\n3. Segitiga sama kaki\n4. Segitiga sama sisi\n5. Segitiga istimewa\n6. Lingkaran\n0. Menu"
        return msg
        lock.release()
    server.register_function(keliling)

    #  buat fungsi bernama basic
    def tambah(a,b):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a+b
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(tambah)
    def kurang(a,b):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a-b
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(kurang)
    def kali(a,b):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a*b
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(kali)
    def bagi(a,b):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a/b
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(bagi)

    def persegi(s):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return s*s
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(persegi)
    def persegi_panjang(p,l):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return p*l
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(persegi_panjang)
    def segitiga(a,t):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return 0.5*a*t
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(segitiga)
    def lingkaran(r):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return math.pi*r*r
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(lingkaran)

    def persegikel(s):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return s*4
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(persegikel)
    def persegi_panjangkel(p,l):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return (2*p)+(l*2)
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(persegi_panjangkel)
    def segitigasamakakikel(a,t):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a+(2*t)
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(segitigasamakakikel)
    def segitigasamasisikel(a):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a*3
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(segitigasamasisikel)
    def segitigaistimewakel(a,b,c):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return a+b+c
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(segitigasamasisikel)
    def lingkarankel(r):
        # critical section dimulai harus dilock
        lock.acquire()
        lock.release()
        return math.pi*(2*r)
        # critical section berakhir, harus diunlock
        lock.release()
    server.register_function(lingkarankel)

    print("Server CAOL Running...")
    # Jalankan server
    server.serve_forever()
