from mpi4py import MPI
jum=10000
def ulangi(jum,mulai,stop):
    s = 1.0/jum
    tot = 0
    for i in range(mulai,stop):
        x= (i+0.5)*s
        tot += 4.0/(1.0+x*x)
    print (tot)
    return tot    
com = MPI.COMM_WORLD
r = com.Get_rank()
s = com.Get_size()
jumlah = jum//s
total = ulangi(jum,r*jumlah,(r+1)*jumlah)
tot = com.reduce(total, op=MPI.SUM, root=0)
if not r:
    pi = tot / jum
    print (pi)