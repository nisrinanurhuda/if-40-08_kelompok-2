from mpi4py import MPI
import random
com = MPI.COMM_WORLD
r = com.Get_rank()
s = com.Get_size()
n = random.randint(0,100)
print ("Rank %d : %d" %(r, n))
total = com.reduce(n, op=MPI.SUM, root=0)
if not r:
	print ("total %d" %total)
