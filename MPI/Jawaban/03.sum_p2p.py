from mpi4py import MPI
import random
com = MPI.COMM_WORLD
r=com.Get_rank()
s=com.Get_size()
n = random.randint(0,100)
print ("Rank %d : %d" %(r, n))
if not r:
	total = n
	for i in range(1,s):
		total += com.recv(source=i)
	print ("total %d" %(total))
else:
	com.send(n, dest=0)